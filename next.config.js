module.exports = {
	reactStrictMode: true,
	i18n: {
		locales: ['fr', 'en'],
		defaultLocale: 'en',
		domains: [
			{domain: 'i-do-not-exists.vercel.app', defaultLocale: 'fr'},
			{domain: 'tests-nextjs.vercel.app', defaultLocale: 'en'},
		],
		localeDetection: false,
	},
	async rewrites() {
		return [
			{
				source: '/js/index.js',
				destination: 'https://plausible.io/js/plausible.outbound-links.js',
				basePath: false
			},
			{
				source: '/api/event', // Or '/api/event/' if you have `trailingSlash: true` in this config
				destination: 'https://plausible.io/api/event',
				basePath: false
			}
		];
	}
}
